package br.digio.sandbox.data.remote

import br.digio.sandbox.domain.model.Home
import br.digio.sandbox.feature.home.business.data.HomeData
import retrofit2.http.GET

interface API {

    @GET("sandbox/products")
    suspend fun getHome(): Home
}