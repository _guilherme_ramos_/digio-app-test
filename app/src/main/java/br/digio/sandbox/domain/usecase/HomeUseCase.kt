package br.digio.sandbox.domain.usecase

import br.digio.sandbox.core.domain.UseCase
import br.digio.sandbox.domain.model.Home
import br.digio.sandbox.domain.repository.HomeRepository
import javax.inject.Inject

class HomeUseCase @Inject constructor(
    private val homeRepository: HomeRepository
) : UseCase<Unit, Home>() {

    override suspend fun execute(parameters: Unit): Home {
        return homeRepository.getHome()
    }
}