package br.digio.sandbox.domain.repository

import br.digio.sandbox.data.remote.API
import br.digio.sandbox.domain.model.Home
import javax.inject.Inject

interface HomeRepository {
    suspend fun getHome(): Home
}

class HomeRepositoryImpl @Inject constructor(
    private val api: API
) : HomeRepository {

    override suspend fun getHome(): Home {
        return api.getHome()
    }
}