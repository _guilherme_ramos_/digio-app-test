package br.digio.sandbox.domain.model

data class Spotlight(
    val name: String,
    val bannerURL: String,
    val description: String
)