package br.digio.sandbox.domain.model

data class Home(
    val spotlight: List<Spotlight> = emptyList(),
    val products: List<Product> = emptyList(),
    val cash: Cash = Cash()
)