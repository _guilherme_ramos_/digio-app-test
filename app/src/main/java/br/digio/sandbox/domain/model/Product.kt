package br.digio.sandbox.domain.model

data class Product(
    val name: String,
    val imageURL: String,
    val description: String
)