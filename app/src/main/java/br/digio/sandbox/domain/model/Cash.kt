package br.digio.sandbox.domain.model

class Cash(
    val title: String = "",
    val bannerURL: String = "",
    val description: String = ""
)