package br.digio.sandbox.di.application

import br.digio.sandbox.MainApplication
import br.digio.sandbox.di.module.AppModule
import br.digio.sandbox.di.module.FragmentModule
import br.digio.sandbox.di.module.NetworkModule
import br.digio.sandbox.di.module.RepositoryModule
import br.digio.sandbox.di.module.ViewModelModule
import br.digio.sandbox.di.viewmodel.ViewModelFactoryModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        FragmentModule::class,
        AppModule::class,
        ViewModelFactoryModule::class,
        ViewModelModule::class,
        RepositoryModule::class,
        NetworkModule::class
    ]
)
interface AppComponent : AndroidInjector<MainApplication> {

    @Component.Factory
    interface Factory {
        fun create(@BindsInstance application: MainApplication): AppComponent
    }
}