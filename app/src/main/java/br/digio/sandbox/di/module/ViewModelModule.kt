package br.digio.sandbox.di.module

import androidx.lifecycle.ViewModel
import br.digio.sandbox.di.viewmodel.ViewModelKey
import br.digio.sandbox.ui.home.HomeViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    abstract fun bindHomeViewModel(viewModel: HomeViewModel): ViewModel
}