package br.digio.sandbox.di.module

import br.digio.sandbox.domain.repository.HomeRepository
import br.digio.sandbox.domain.repository.HomeRepositoryImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class RepositoryModule {

    @Binds
    @Singleton
    abstract fun bindHomeRepository(homeRepositoryImpl: HomeRepositoryImpl): HomeRepository
}