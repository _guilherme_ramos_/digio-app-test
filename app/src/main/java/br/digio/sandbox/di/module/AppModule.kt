package br.digio.sandbox.di.module

import android.content.Context
import br.digio.sandbox.MainApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    fun providesContext(application: MainApplication): Context {
        return application.applicationContext
    }
}
