package br.digio.sandbox.di.module

import br.digio.sandbox.core.ui.StateFragment
import br.digio.sandbox.ui.home.HomeFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentModule {

    @ContributesAndroidInjector
    abstract fun contributeHomeFragment(): HomeFragment

    @ContributesAndroidInjector
    abstract fun contributeStateFragment(): StateFragment
}