package br.digio.sandbox.core.domain

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import br.digio.sandbox.core.exception.DigioException
import br.digio.sandbox.core.result.Result
import java.io.IOException

abstract class UseCase<in P, R> {
    suspend operator fun invoke(parameters: P, result: MutableLiveData<Result<R>>) {
        result.postValue(Result.Loading)

        val resultExecute = try {
            Result.Success(execute(parameters))
        } catch (e: IOException) {
            Result.Error(DigioException.DigioConnectionError)
        } catch (e: Exception) {
            Result.Error(DigioException.DigioGenericException)
        }

        result.postValue(resultExecute)
    }

    suspend operator fun invoke(parameters: P): LiveData<Result<R>> {
        val liveCallback: MutableLiveData<Result<R>> = MutableLiveData()
        this(parameters, liveCallback)
        return liveCallback
    }

    @Throws(RuntimeException::class)
    protected abstract suspend fun execute(parameters: P): R
}
