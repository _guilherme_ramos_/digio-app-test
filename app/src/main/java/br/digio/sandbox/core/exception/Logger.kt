package br.digio.sandbox.core.exception

import android.util.Log

private const val LOGGER_TAG = "DIGIO APP SANDBOX"

fun logMessage(message: String) {
    Log.d(LOGGER_TAG, message)
}