package br.digio.sandbox.core.result

import androidx.annotation.RawRes
import androidx.annotation.StringRes
import br.digio.sandbox.R

sealed class ErrorStates(
    @StringRes val message: Int,
    @RawRes val lottieFile: Int
) {
    object GenericErrorState : ErrorStates(
        R.string.default_states_generic_error_message,
        R.raw.lottie_empty_state
    )

    object NetworkErrorState : ErrorStates(
        R.string.default_states_network_error_message,
        R.raw.lottie_no_connection
    )
}