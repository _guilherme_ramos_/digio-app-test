package br.digio.sandbox.core.view

import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.observe

fun <T> Fragment.onObserve(eventSource: LiveData<T>, handler: (T) -> Unit) {
    eventSource.observe(viewLifecycleOwner) { handler(it) }
}


