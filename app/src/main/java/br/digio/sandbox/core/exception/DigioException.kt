package br.digio.sandbox.core.exception

import br.digio.sandbox.BuildConfig
import br.digio.sandbox.core.result.ErrorStates

sealed class DigioException(val message: String, val errorStates: ErrorStates) {
    object DigioConnectionError : DigioException(
        BuildConfig.CONNECTION_EXCEPTION_MESSAGE,
        ErrorStates.NetworkErrorState
    )

    object DigioGenericException : DigioException(
        BuildConfig.GENERIC_EXCEPTION_MESSAGE,
        ErrorStates.GenericErrorState
    )
}
