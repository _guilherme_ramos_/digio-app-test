package br.digio.sandbox.core.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import br.digio.sandbox.R
import br.digio.sandbox.core.result.ErrorStates
import com.airbnb.lottie.LottieAnimationView
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.content_generic_state.*

class StateFragment : DaggerFragment() {

    private lateinit var configuration: ErrorStates
    private val args: StateFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.content_generic_state, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mapArgument()
        setUpScreen()
    }

    private fun setUpScreen() {
        content_generic_text.text = getString(configuration.message)

        (content_generic_lottie as LottieAnimationView).run {
            setAnimation(configuration.lottieFile)
            playAnimation()
        }
    }

    private fun mapArgument() {
        configuration = if (ErrorStates.GenericErrorState.javaClass.name == args.type)
            ErrorStates.GenericErrorState
        else
            ErrorStates.NetworkErrorState
    }
}