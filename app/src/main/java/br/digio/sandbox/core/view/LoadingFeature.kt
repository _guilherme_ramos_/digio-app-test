package br.digio.sandbox.core.view

interface LoadingFeature {
    fun setLoadingVisible(state: Boolean)
}