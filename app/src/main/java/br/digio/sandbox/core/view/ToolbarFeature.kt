package br.digio.sandbox.core.view

interface ToolbarFeature {
    fun setTitle(title: String)
}