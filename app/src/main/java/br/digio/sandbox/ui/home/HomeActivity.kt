package br.digio.sandbox.ui.home

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import br.digio.sandbox.R
import br.digio.sandbox.core.view.LoadingFeature
import br.digio.sandbox.core.view.ToolbarFeature
import br.digio.sandbox.ui.helper.showIf
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity(), LoadingFeature, ToolbarFeature {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setSupportActionBar(main_toolbar)
    }

    override fun setLoadingVisible(state: Boolean) {
        loading_container.showIf(state)
    }

    override fun setTitle(title: String) {
        supportActionBar?.title = title
    }
}