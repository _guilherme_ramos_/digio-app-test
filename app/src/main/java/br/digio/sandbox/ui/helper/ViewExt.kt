package br.digio.sandbox.ui.helper

import android.view.View
import android.widget.ImageView
import br.digio.sandbox.R
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy

fun View.showIf(visible: Boolean, keepInLayout: Boolean = false) {
    if (visible) show() else hide(keepInLayout)
}

fun View.show() {
    visibility = View.VISIBLE
}

fun View.hide(keepInLayout: Boolean = false) {
    visibility = if (keepInLayout) View.INVISIBLE else View.GONE
}

fun ImageView.loadUrl(url: String?) {
    Glide.with(context)
        .load(url)
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .error(R.drawable.img_logo_default)
        .into(this)
}
