package br.digio.sandbox.ui.home

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import br.digio.sandbox.R
import br.digio.sandbox.domain.model.Spotlight
import br.digio.sandbox.ui.helper.inflate
import br.digio.sandbox.ui.helper.loadUrl
import kotlinx.android.synthetic.main.item_home_spotlight.view.*

class SpotlightAdapter :
    ListAdapter<Spotlight, SpotlightAdapter.ViewHolder>(SpotlightDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.item_home_spotlight))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
    }

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(position: Int) {
            val item = getItem(position)
            view.item_home_spotlight_image.loadUrl(item.bannerURL)
        }
    }
}

private class SpotlightDiffCallback : DiffUtil.ItemCallback<Spotlight>() {
    override fun areItemsTheSame(oldItem: Spotlight, newItem: Spotlight): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Spotlight, newItem: Spotlight): Boolean {
        return oldItem == newItem
    }
}