package br.digio.sandbox.ui.home

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import androidx.lifecycle.viewModelScope
import br.digio.sandbox.core.exception.logMessage
import br.digio.sandbox.core.result.ErrorStates
import br.digio.sandbox.core.result.Event
import br.digio.sandbox.core.result.Result
import br.digio.sandbox.core.result.succeeded
import br.digio.sandbox.core.result.successOr
import br.digio.sandbox.domain.model.Home
import br.digio.sandbox.domain.usecase.HomeUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class HomeViewModel @Inject constructor(
    private val homeUseCase: HomeUseCase
) : ViewModel() {

    private val _homeViewState = MutableLiveData<Result<Home>>()

    val loadingObserver = _homeViewState.map {
        it == Result.Loading
    }

    val homeObserver = MediatorLiveData<Home>()
        .apply {
            addSource(_homeViewState) {
                if (it.succeeded) {
                    value = it.successOr(Home())
                }
            }
        }

    val messageObserver = MediatorLiveData<Event<ErrorStates>>()
        .apply {
            addSource(_homeViewState) {
                logErrorMessage(it)
            }
        }

    fun onViewCreated() {
        callScopeFunction { homeUseCase.invoke(Unit, _homeViewState) }
    }

    private fun MediatorLiveData<*>.logErrorMessage(result: Result<*>) {
        if (result is Result.Error) {
            logMessage(result.appException.message)
            value = Event(result.appException.errorStates)
        }
    }

    private fun callScopeFunction(executer: suspend () -> Unit) {
        viewModelScope.launch(Dispatchers.IO) { executer() }
    }
}