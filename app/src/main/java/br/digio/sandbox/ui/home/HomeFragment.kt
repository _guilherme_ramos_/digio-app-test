package br.digio.sandbox.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment.findNavController
import br.digio.sandbox.R
import br.digio.sandbox.core.result.ErrorStates
import br.digio.sandbox.core.result.Event
import br.digio.sandbox.core.view.LoadingFeature
import br.digio.sandbox.core.view.ToolbarFeature
import br.digio.sandbox.core.view.onObserve
import br.digio.sandbox.domain.model.Home
import br.digio.sandbox.ui.helper.loadUrl
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_home.*
import javax.inject.Inject

class HomeFragment : DaggerFragment() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: HomeViewModel by viewModels { viewModelFactory }

    private lateinit var spotlightAdapter: SpotlightAdapter
    private lateinit var productsAdapter: ProductsAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpLists()
        setUpViewModel()
        viewModel.onViewCreated()
    }

    private fun setUpLists() {
        spotlightAdapter = SpotlightAdapter()
        home_spotlight_list.adapter = spotlightAdapter

        productsAdapter = ProductsAdapter()
        home_products_list.adapter = productsAdapter
    }

    private fun setUpViewModel() {
        onObserve(viewModel.loadingObserver, ::handleLoading)
        onObserve(viewModel.homeObserver, ::setUpHomeData)
        onObserve(viewModel.messageObserver, ::handleMessages)
    }

    private fun setUpHomeData(data: Home?) {
        handleUserName()
        handleHomeData(data)
    }

    private fun handleHomeData(data: Home?) {
        data?.run {
            spotlightAdapter.submitList(spotlight)
            productsAdapter.submitList(products)
            item_home_cash_image.loadUrl(cash.bannerURL)
        }
    }

    private fun handleMessages(message: Event<ErrorStates>) {
        message.getContentIfNotHandled()?.run {
            navigateToErrorState(this)
        }
    }

    private fun handleLoading(showLoading: Boolean) {
        (requireActivity() as LoadingFeature).setLoadingVisible(showLoading)
    }

    private fun handleUserName() {
        (requireActivity() as ToolbarFeature).setTitle("Olá, Maria")
    }

    private fun navigateToErrorState(errorStates: ErrorStates) {
        val action = HomeFragmentDirections.mainToState(errorStates.javaClass.name)
        findNavController(this).navigate(action)
    }
}