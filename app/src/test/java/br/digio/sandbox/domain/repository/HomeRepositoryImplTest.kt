package br.digio.sandbox.domain.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import br.digio.sandbox.data.remote.API
import br.digio.sandbox.domain.model.Home
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import java.lang.RuntimeException

@RunWith(MockitoJUnitRunner::class)
class HomeRepositoryImplTest {

    @Mock
    private lateinit var api: API

    private lateinit var homeRepository: HomeRepository

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        homeRepository = HomeRepositoryImpl(api)
    }

    @Test
    fun `when request home assert returned value`() = runBlocking {
        val expectedResult = Home()

        `when`(api.getHome()).thenReturn(expectedResult)

        val result = homeRepository.getHome()

        verify(api, times(1)).getHome()
        assertEquals(expectedResult, result)
    }
}