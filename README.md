# Digio App Test #

Repositório criado para o teste da Digio

### Design ###

O aplicativo foi feito com uma organização de arquivos favoráveis para o desenvolvimento da funcionalidade
de `tema escuro (night mode)`. Isso se deve a aplicação de temas e estilos de acordo com o sugerido pelo
Google no [DevSummit](https://www.youtube.com/watch?v=Owkf8DhAOSo)

Para melhorar a interface, foi desenvolvido telas genéricas de erros com animações usando _Lottie_.

O logo do aplicativo foi desenhado utilizando a ferramenta _Figma_ (não foi utilizado o logo da Digio)

As fontes são da família _Montserrat_

### Arquitetura ###

Visando a criação de testes e divisão de responsabilidades, foi usado _MVVM + Clean Arch_. Tal arquitetura
também facilita no acoplamento de novas funcionalidades externas e mudularização do projeto.

Segue um vocabulário relevante para se familiarizar e entender melhor tal abordagem:

- **Models**: objetos de negócios do aplicativo.
- **Use Cases or Repositories**: casos de uso orquestram o fluxo de dados para as entidades.
- **Interface Adapters**: conjunto de `adaptadores` que convertem os dados para o formato mais conveniente. Presenters, Controllers e MVVM ficam aqui.
- **Frameworks and Drivers**: interface do usuário, ferramentas, estruturas etc.

Além disso, foi utilizado Navigations para a navegação entre telas.

### Débitos técnicos ###

Em relação aos testes:
* Teste instrumentado
* Cobertura de testes unitários (foi desenvolvido apenas uma classe de teste como exemplo)

Em relação a arquitetura:
* Melhorias de fallbacks utilizando armazenamento de temporário de dados com o Room

Em relação a design:
* Substituição para ViewStub em telas de estados genéricos de _loading_ e erros

Contato
[LinkedIn](https://www.linkedin.com/in/guilherme-henrique-ramos-da-silva/)
[E-mail](guilhermeramos.dev@gmail.com)
